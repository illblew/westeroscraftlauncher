package org.mcupdater;

import static java.io.File.pathSeparator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JWindow;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.w3c.dom.Document;

public class BootstrapForm extends JWindow
	implements TrackerListener {
	private static final ResourceBundle config = ResourceBundle.getBundle("config"); //$NON-NLS-1$
	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblStatus;
	private Distribution distro;
	private static File basePath;// = new File("/home/sbarbour/Bootstrap-test");
	private static PlatformType thisPlatform;
	private String[] passthroughParams;
	private Color CLEAR = new Color(0.0F, 0.0F, 0.0F, 0.0F);
	
	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		String customPath = config.getString("customPath");
		if(System.getProperty("os.name").startsWith("Windows"))
		{
			basePath = new File(new File(System.getenv("APPDATA")),"WesterosCraft");
			thisPlatform = PlatformType.valueOf("WINDOWS" + System.getProperty("sun.arch.data.model"));
		} else if(System.getProperty("os.name").startsWith("Mac"))
		{
			basePath = new File(new File(new File(new File(System.getProperty("user.home")),"Library"),"Application Support"),"WesterosCraft");
			thisPlatform = PlatformType.valueOf("OSX64");
		}
		else
		{
			basePath = new File(new File(System.getProperty("user.home")),"WesterosCraft");
			thisPlatform = PlatformType.valueOf("LINUX" + System.getProperty("sun.arch.data.model"));
		}
		if (!customPath.isEmpty()) {
			basePath = new File(customPath);
		}
		File launcherLog = new File(basePath, "launcher.log");
		try {
            PrintStream log = new PrintStream(launcherLog);
            System.setOut(log);
            System.setErr(log);
        } catch (IOException e1) {
            System.out.println("Unable to redirect to log");
        }

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
						//System.out.println(info.getName() + " : " + info.getClassName());
				        if ("Nimbus".equals(info.getName())) {
				            UIManager.setLookAndFeel(info.getClassName());
				            break;
				        }
				    }
					if (UIManager.getLookAndFeel().getName().equals("Metal")) {
						UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					}
					BootstrapForm frame = new BootstrapForm();
					frame.setPassthroughParams(args);
					frame.setLocationRelativeTo( null );
					frame.setVisible(true);
					frame.doWork();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void setPassthroughParams(String[] args) {
		this.passthroughParams = args;
	}
	
	protected void doWork() {
	    SwingWorker<Distribution, Void> worker = new SwingWorker<Distribution, Void>() {
	        @Override
	        public Distribution doInBackground() {
	            Distribution dist = null;
	            File distroDocCache = new File(basePath, "distrodoc.xml");
	            Document distroDoc = null;
	            try {
	                distroDoc = DistributionParser.readXmlFromUrl(config.getString("bootstrapURL"));
	                if (distroDoc != null) {  // Loaded?
	                    try {   // Save in cache
	                        TransformerFactory transformerFactory = TransformerFactory.newInstance();
	                        Transformer transformer;
	                        transformer = transformerFactory.newTransformer();
	                        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	                        DOMSource source = new DOMSource(distroDoc);
	                        // Write to file
	                        StreamResult file = new StreamResult(distroDocCache);
	                        transformer.transform(source, file);
	                    } catch (TransformerConfigurationException e) {
	                        System.out.println("Error encoding distribution XML : " + e.getLocalizedMessage());
	                    } catch (TransformerException e) {
	                        System.out.println("Error encoding distribution XML : " + e.getLocalizedMessage());
	                    }
	                }
	            } catch (Exception e1) {
	                System.out.println("Error loading distribution document: checking for cached copy");
	            }
	            // If not loaded, and we have cache file
	            if ((distroDoc == null) && distroDocCache.exists()) {
                    try {
                        distroDoc = DistributionParser.readXmlFromFile(distroDocCache);
                        if (distroDoc != null) {
                            System.out.println("Loaded cached copy of distribution document from " + distroDocCache.getPath());
                        }
                    } catch (Exception e) {
                        System.out.println("Failed to load cached copy of distribution document from " + distroDocCache.getPath() + " : " + e.getLocalizedMessage());
                    }
                }
	            if (distroDoc != null) {
	                dist = DistributionParser.parseDocument(distroDoc, config.getString("distribution"), System.getProperty("java.version").substring(0,3), thisPlatform);
	            }
	            return dist;
	        }

	        @Override
	        public void done() {
	            try {
	                distro = get();
	            }
	            catch (InterruptedException ignore) {
	            }
	            catch (java.util.concurrent.ExecutionException e) {
	            }
	            if (distro == null) {
	                JOptionPane.showMessageDialog(BootstrapForm.this, "Failed to read configured distribution!","MCU-Bootstrap",JOptionPane.ERROR_MESSAGE);
	                System.exit(-1);
	            }
	            updateStatus("Downloading " + distro.getFriendlyName());
	            Collection<Downloadable> dl = new ArrayList<Downloadable>();
	            for (Library l : distro.getLibraries()) {
	                Downloadable dlEntry = new Downloadable(l.getName(),l.getFilename(),l.getMd5(),l.getSize(),l.getDownloadURLs());
	                dl.add(dlEntry);
	            }
	            DownloadQueue queue = new DownloadQueue("Bootstrap", "Bootstrap", BootstrapForm.this, dl, new File(basePath,"lib"), null);
	            queue.processQueue(new ThreadPoolExecutor(0, 1, 500, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()));      
	        }
	    };
// *** Debug section
		System.out.println("System.getProperty('os.name') == '" + System.getProperty("os.name") + "'");
		System.out.println("System.getProperty('os.version') == '" + System.getProperty("os.version") + "'");
		System.out.println("System.getProperty('os.arch') == '" + System.getProperty("os.arch") + "'");
		System.out.println("System.getProperty('java.version') == '" + System.getProperty("java.version") + "'");
		System.out.println("System.getProperty('java.vendor') == '" + System.getProperty("java.vendor") + "'");
		System.out.println("System.getProperty('sun.arch.data.model') == '" + System.getProperty("sun.arch.data.model") + "'");
// ***
	    worker.execute();
	}

	private int timer = 0;
	/**
	 * Create the frame.
	 */
	public BootstrapForm() {
		// setBounds(100, 100, 450, 300);
        setBackground(CLEAR);
		contentPane = new JPanel();
		contentPane.setBorder(BorderFactory.createEmptyBorder());
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.setBackground(CLEAR);
        setContentPane(contentPane);
		
		JPanel progressPanel = new JPanel();
		progressPanel.setBorder(new EmptyBorder(3, 0, 0, 0));
		contentPane.add(progressPanel, BorderLayout.SOUTH);
		progressPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel primaryProgress = new JPanel();
		progressPanel.add(primaryProgress, BorderLayout.CENTER);
		primaryProgress.setLayout(new BorderLayout(0, 0));
				
		lblStatus = new JLabel("Starting!");
		primaryProgress.add(lblStatus, BorderLayout.SOUTH);

        final ImageIcon imageBG = new ImageIcon(BootstrapForm.class.getResource("/org/mcupdater/westeroscraftlogo1.png"));
        final ImageIcon imageFG = new ImageIcon(BootstrapForm.class.getResource("/org/mcupdater/westeroscraftlogo2.png"));

		final JPanel logoPanel = new JPanel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 8686753828984892019L;
			public ImageIcon image = imageBG;
			public ImageIcon image2 = imageFG;
			private double getAngle() {
			    return Math.toRadians(2 * timer);
			}
			@Override
			protected void paintComponent(Graphics g) {
				Image source = this.image.getImage();
                Image source2 = this.image2.getImage();
				int w = source.getWidth(null);
				int h = source.getHeight(null);
				//BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
			    GraphicsConfiguration gc = getGraphicsConfiguration();
			    Image image = gc.createCompatibleImage(w, h, Transparency.TRANSLUCENT);	
			    Graphics2D g2d = (Graphics2D)image.getGraphics();
				g2d.drawImage(source, 0, 0, null);
	            AffineTransform at = new AffineTransform();
	            at.setToRotation(getAngle(), (w / 2), (h / 2));
	            at.translate(0, 0);
	            g2d.setTransform(at);
                g2d.drawImage(source2, 0, 0, null);
				g2d.dispose();
		        int width = getWidth();  
		        int height = getHeight();  
		        int imageW = image.getWidth(this);  
		        int imageH = image.getHeight(this);  
		        // Compute ratios
		        float ratioW = (float)width / (float) imageW;
                float ratioH = (float)height / (float) imageH;
                // Pick the smaller
                if (ratioW < ratioH) {
                    int extra = (height - (int)(imageH*ratioW)) / 2;
                    g.drawImage(image, 0, extra, width, (int)(imageH*ratioW) + extra, 0, 0, imageW, imageH, this);
                }
                else {
                    int extra = (width - (int)(imageW * ratioH)) / 2;
                    g.drawImage(image, extra, 0, (int)(imageW * ratioH) + extra, height, 0, 0, imageW, imageH, this);
                }
			}			
		};
		Timer t = new Timer(1000 / 50, new ActionListener() {
		    public void actionPerformed(ActionEvent ae) {
		        timer++;
		        logoPanel.repaint();
		    }
		});
        logoPanel.setBackground(CLEAR);
        logoPanel.setBorder(BorderFactory.createEmptyBorder());
		contentPane.add(logoPanel, BorderLayout.CENTER);
		//logoPanel.setLayout(new BorderLayout(0, 0));
				
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        // Compute ratios
		int imgW = imageBG.getIconWidth();
        int imgH = imageBG.getIconHeight();
        
        float ratioW = (float)gd.getDisplayMode().getWidth() / (float) imgW;
        float ratioH = (float)gd.getDisplayMode().getHeight() / (float) imgH;
        // Pick the smaller
        if (ratioW < ratioH) {    
            setSize((gd.getDisplayMode().getWidth() * 2) / 5, (int)(imgH * ratioW * 0.4));
        }
        else {
            setSize((int)(imgW * ratioH * 0.4), (gd.getDisplayMode().getHeight() * 2) / 5);
        }
        t.start();

	}

	@Override
	public void onQueueFinished(DownloadQueue queue) {
		if (queue.getFailedFileCount() > 0) {
			updateStatus("Failed!");
			final StringBuilder msg = new StringBuilder("Failed to download:\n");
			for (Downloadable entry : queue.getFailures()) {
				msg.append("   " + entry.getFilename() + "\n");
			}
			EventQueue.invokeLater(new Runnable() {
	            public void run() {
	                JOptionPane.showMessageDialog(BootstrapForm.this, msg.toString(),"MCU-Bootstrap",JOptionPane.ERROR_MESSAGE);
	                BootstrapForm.this.dispose();
	                System.exit(-2);
	            }
	       });
		} else {
		    updateStatus("Finished!");
            boolean legacy = distro.getMainClass().equals("org.mcupdater.MainShell"); // Is legacy core?
			StringBuilder sbClassPath = new StringBuilder();
			for (Library lib : distro.getLibraries()){
				sbClassPath.append(cpDelimiter() + (new File(new File(basePath, "lib"), lib.getFilename())).getAbsolutePath());
			}
			if (!legacy) {
			    File jfxrt = new File(System.getProperty("java.home") + File.separator + "lib/jfxrt.jar");
			    if (jfxrt.exists()) {
			        sbClassPath.append(pathSeparator).append(System.getProperty("java.home")).append(File.separator).append("lib/jfxrt.jar");
			    }
			}
			StringBuilder sbParams = new StringBuilder();
			sbParams.append(distro.getParams());
			try {
				String javaBin = "java";
				File binDir;
				if (System.getProperty("os.name").startsWith("Mac")) {
					binDir = new File(new File(System.getProperty("java.home")), "Commands");
				} else {
					binDir = new File(new File(System.getProperty("java.home")), "bin");
				}
				if( binDir.exists() ) {
					javaBin = (new File(binDir, "java")).getAbsolutePath();
				}
				List<String> args = new ArrayList<String>();
				args.add(javaBin);
				if (!legacy) {
				    args.add("-Djavafx.verbose=true");
				}
                if (System.getProperty("os.name").toUpperCase().equals("MAC OS X")) {
                    if (legacy) {
                        args.add("-XstartOnFirstThread");
                        args.add("-d64");
                    }
                    args.add("-Xdock:name=" + distro.getFriendlyName());
                    args.add("-Xdock:icon=" + (new File(new File(basePath, "lib"), "mcu-icon.icns")).getAbsolutePath());
                }
				args.add("-cp");
				args.add(sbClassPath.toString().substring(1));
				args.add(distro.getMainClass());
				Map<String,String> fields = new HashMap<String,String>();
				StrSubstitutor fieldReplacer = new StrSubstitutor(fields);
				fields.put("defaultPack", config.getString("defaultPack"));
				fields.put("MCURoot", basePath.getAbsolutePath());
				//if (distro.getParams() != null) { args.addAll(Arrays.asList(fieldReplacer.replace(distro.getParams()).split(" ")));}
				if (distro.getParams() != null) {
					String[] fieldArr = distro.getParams().split(" ");
					for (int i = 0; i < fieldArr.length; i++) {
						fieldArr[i] = fieldReplacer.replace(fieldArr[i]);
					}
					args.addAll(Arrays.asList(fieldArr));					
				}

				args.addAll(Arrays.asList(this.passthroughParams));
				String[] params = args.toArray(new String[0]);
				for (String s : args) {
					System.out.print(s + " ");
				}
				System.out.print("\n");

				Runtime.getRuntime().exec(params);
                Thread.sleep(3000);
                EventQueue.invokeAndWait(new Runnable() {
                    public void run() {
                        BootstrapForm.this.dispose();
                    }
                });
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } finally {
                System.exit(0);
			}
		}
	}

	@Override
	public void onQueueProgress(final DownloadQueue queue) {
	    updateStatus("Downloading: " + queue.getName());
	}

	private void updateStatus(final String msg) {
	    if (EventQueue.isDispatchThread()) {
            lblStatus.setText(msg);
	    }
	    else {
	        EventQueue.invokeLater(new Runnable() {
	            public void run() {
	                lblStatus.setText(msg);
	            }
	        });;
	    }
	}
	
	@Override
	public void printMessage(String msg) {
		System.out.println(msg);
	}

	private String cpDelimiter() {
		String osName = System.getProperty("os.name");
		if (osName.startsWith("Windows")) {
			return ";";
		} else {
			return ":";
		}
	}
}
