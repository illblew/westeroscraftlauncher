package com.westeroscraft;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

public class JavaVersionError extends JDialog implements ActionListener {
    public JavaVersionError(JFrame parent, String title, String message) {
      super(parent, title, true);
      JPanel messagePane = new JPanel();
      messagePane.add(new JLabel(message));
      getContentPane().add(messagePane);
      JPanel buttonPane = new JPanel();
      JButton button = new JButton("OK"); 
      buttonPane.add(button); 
      button.addActionListener(this);
      getContentPane().add(buttonPane, BorderLayout.SOUTH);
      setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      pack(); 
      GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
      setLocation((gd.getDisplayMode().getWidth() - this.getWidth()) / 2, (gd.getDisplayMode().getHeight() - this.getHeight()) / 2);
      setVisible(true);
    }
    public void actionPerformed(ActionEvent e) {
      setVisible(false); 
      dispose(); 
      System.exit(0);
    }
    public static void main(String[] a) {
        JavaVersionError dlg = new JavaVersionError(new JFrame(), "WesterosCraft Launcher Error", "WesterosCraft Launcher requires Java 7 or 8!");
    }
  }
